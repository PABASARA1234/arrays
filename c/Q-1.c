#include <stdio.h>
#include <string.h>

void printReverse(char str[]){

    int size = strlen(str);
    int i;
    for (i = size - 1; i >= 0; i--) {
        if (str[i] == ' ') {
            str[i] = '\0';
            printf("%s ", &(str[i]) + 1);
        }
    }
    printf("%s", str);
}
int main(){
    char str[10];
    printf("Enter the sentence  :");
    scanf("%[^\n]%*c", &str);
    printReverse(str);
    return 0;
}

